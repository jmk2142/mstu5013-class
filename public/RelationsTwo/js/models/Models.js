// TODO MODEL
var Todo = Parse.Object.extend('Todo', {
	defaults: {
		user: undefined,
		author: undefined,
		content: 'Nothing',
		comments: [],
		recentComment: {
			author: undefined,
			content: 'No Comments' 
		},
		commentCount: 0,
		status: 'incomplete'
	},
	initialize: function() {
		console.log('init todo');
	}
});

// COMMENT MODEL
var Comment = Parse.Object.extend('Comment', {
	defaults: {
		user: undefined,
		author: undefined, // Redundant? But useful... can print name without including nested user Parse Object
		content: 'Nothing'
	},
	initialize: function() {
		console.log('init comment');
	}
});