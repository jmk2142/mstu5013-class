var AppView = Parse.View.extend({
	events: {
		'click #addTodoItem': 'makeTodo'
	},
	initialize: function() {
		// Remember, we passed in an instance of a collection
		// which we can refer to as this.collection inside this view

		// We can have the collection LISTEN to an 'add' event
		// The add event is something baked into collections
		// and will trigger any time a model is added.

		// When the event is triggered, it will execute the callback
		// function, this.addTodo and pass in the model that was added
		// the last arg 'this' makes sure this represents this view
		// and not the collection.
		this.collection.on('add', this.addTodoView, this);

		// We can also listen to the collection reset at the beginning
		// fetch() and addAllTodoViews()
		this.collection.on('reset', this.addAllTodoViews, this);
	},
	makeTodo: function() {
		var fullName = Parse.User.current().get('first') + ' ' + Parse.User.current().get('last');

		var todoText = this.$('#todoInput').val();

		var todoItem = new Todo({
			user: Parse.User.current(),
			author: fullName,
			content: todoText
		});

		// After we create it, we add it to the this.collection
		// much like we would with an array.
		this.collection.add(todoItem);

		// Adding the todoItem to the collection triggers the add
		// event... which we are listening to (see line 15)
		// This causes this view to run the callback, addTodo()
		// Which is a function that adds the todo to the page

		todoItem.save();

		this.$('#todoInput').val('');
	},
	addTodoView: function(todoModel) {

		var todoView = new TodoView({
			model: todoModel
		});

		if (todoModel.isNew()) {
			this.$('#todoList').prepend(todoView.render().el);
		} else {
			this.$('#todoList').append(todoView.render().el);
		}
	},
	addAllTodoViews: function(todoCollection) {
		var that = this;

		this.collection.each(function(todoModel) {
			that.addTodoView(todoModel);
		});
	},
});