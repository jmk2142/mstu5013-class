// Initialize Parse
Parse.initialize("JOiRQ4zeHDhhGbzr4oKw9QmKyrlpTcIH6k6uNzXY", "pnXpWoBz1Vx65MB38SNOVjcHnnQYG6L0jEpnVisJ");

// With real apps, usually we want to go query an intial dataset to render.
// Lets grab at max, 50 recipes in the database
var recipeQuery = new Parse.Query('Recipe');
recipeQuery.limit(50);
recipeQuery.find().then(function(recipes) {
	// If successful, we have a dataset. We might have an [] dataset but
	// that is also a success.
	
	// I'm also making a fake/simple User object to pass in
	// Normally we'd have a logged in Parse.User object

	var me = {
		id: '1234567',
		first: 'Jin',
		last: 'Kuwata',
		email: 'jmk2142@tc.columbia.edu',
		createdAt: new Date()
	};

	// With this data, we can mount the riot entry point.
	riot.mount('cookbook', {
		recipeList: recipes,
		user: me
	});

}, function(error) {
	// If there is an error, this gets called.
	// Think in terms of the server being unplugged kind of error.
	console.log(error.message);
});