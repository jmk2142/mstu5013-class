<one-to-many>
	<div class="page-header">
		<h3>ONE-TO-MANY <small>Pointers and Array of Pointers</small></h3>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form class="">
						<div class="form-group">
							<label>AUTHOR</label>

							<select onchange={ selectAuthor }>
								<option value="">---</option>
								<!-- This is getting long winded... -->
								<option each={ item in authors } value={ item.id } selected={ item.id == author.id }>
									{ item.get('user').get('last') }, { item.get('user').get('first')}
								</option>
							</select>

							<select onchange={ selectAuthor }>
								<option>---</option>
								<!-- This is a bit more concise... -->
								<option each={ item in authorsData } value={ item.objectId } selected={ item.objectId == author.id }>
									{ item.user.last }, { item.user.first }
								</option>
							</select>

							<span>
								The two selects are do the same thing, but are produced by a different approach.
							</span>
						</div>

						<div class="form-group">
							<label>TITLE</label>
							<input id="bookTitle" type="text" name="title" placeholder="Title">

							<label>PUBLISHER</label>
							<select value={ publisher.id } onchange={ selectPublisher }>
								<option value="">---</option>
								<option value={ item.objectId } each={ item in publishersData } selected={ item.objectId == publisher.id }>
									{ item.name }
								</option>
							</select>
							<button type="button" onclick={ addBook }>Add to List</button>
						</div>
					</form>
				</div>
			</div>


		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<h3>Author's Books by Book Query</h3>
			<ul>
				<li each={ item in booksData }>
					{ item.title }
				</li>
			</ul>
		</div>

		<div class="col-md-6">
			<h3>Books included in Author Query</h3>
			<ul>
				<li each={ authorData.books }>
					{console.log(book.title)}
					{ title }
				</li>
			</ul>
		</div>
	</div>

	<script>
		var Author = Parse.Object.extend('Author');
		var Publisher = Parse.Object.extend('Publisher');
		var Book = Parse.Object.extend('Book');

		var that = this;

		this.author = undefined; // Parse Object
		this.authorData = undefined; // toJSON() simple JS object of Parse Object

		this.authors = []; // List of Parse Objects
		this.authorsData = []; // toJSON() simple list of JS objects of Parse Objects

		this.publisher = undefined;
		this.publisherData = undefined;

		this.publishers = [];
		this.publishersData = [];

		this.books = [];
		this.booksData = [];

		this.getAuthors = function() {
			var authorQuery = new Parse.Query('Author');
			authorQuery.equalTo('editable', false);
			authorQuery.include('user');

			authorQuery.find().then(function(authors) {

				// Sort the authors by last name then first name
				authors.sort(function(authorA, authorB){
					 var userA = authorA.get('user').toJSON();
					 var userB = authorB.get('user').toJSON();
					 if (userA.last == userB.last) {
						 return (userA.first > userB.first) ? 1 : (userA.first < userB.first) ? -1 : 0;
					 } else {
						 return (userA.last > userB.last) ? 1 : -1;
					 }
				 });

				// Array of Author Parse Objects (leads to long winded markup)
			  that.authors = authors;

				that.update();
			});
		};

		this.getPublishers = function(){
		  var publisherQuery = new Parse.Query('Publisher');
					publisherQuery.ascending('name');

			publisherQuery.find().then(function(publishers) {
				that.publishers = publishers;
				that.update();
			});
		};

		this.getBooks = function() {
			if (this.author) {
				var promiseA, promiseB;

				// TWO ways to think of one to many queryies
				var queryByBook = new Parse.Query('Book');
						queryByBook.equalTo('author', this.author);

						promiseA = queryByBook.find(); // Get all books

				var queryByAuthor = new Parse.Query('Author');
						queryByAuthor.equalTo('objectId', this.author.id);
						queryByAuthor.include('books'); // Note that nested objects aren't automatically included

						promiseB = queryByAuthor.first(); // Get author, include books

				Parse.Promise.when(promiseA, promiseB).then(function(books, author){
				  // console.dir(books);
					// console.dir(author);

					that.books = books;
					that.author = author;

					that.update();
				});
			}

		};

		this.selectAuthor = function(event) {
			var authorID = event.target.value;
			this.author = _.find(this.authors, function(author) {
			  return author.id === authorID;
			});
			this.getBooks();
		};
		this.selectPublisher = function(event) {
			var publisherID = event.target.value;
			this.publisher = _.find(this.publishers, function(publisher) {
				return publisher.id === publisherID;
			});
		};

		/*
			In this example you'll notice that I do two things. One is that I add an author to a book.
			This alone is sufficient for one-to-many type of relationships. I can get all the books
			of a particular author by querying books and making sure the author property matches
			the author object that I'm concerned about.

			I do an extra second thing just as an example. If I'm interested in being able to grab
			an author's books by querying an Author, I could have an author property of books as an array
			of Book objects. E.g. author.books = [<pointers>, ...] In this case, if I query an author
			I can also get all the books by doing an .include() in the query.

			You might use one, or both depending on how you want to query your data. When possible, I would
			opt for the FIRST choice, just including an author as part of book and any time I need all books
			of a particular author, running a Books query that .equalTo('author', AUTHOR)

			This way, if I delete a book - there is no reference to that book left over in any other object.
			Imagine deleting a book but if the Author object still had a <pointer> pointing to a now non-existent
			book. So for cleanup sake, it tends to be nicer to not reference objects all over the place.
		*/
		this.addBook = function(event) {
			var title = this.bookTitle.value;

			if (title && this.author && this.publisher) {

				// One way - book to author is the one to one...
				// when we query, we query all books that match the author
				var book = new Book({
					author: this.author, // what links author to book
					publisher: this.publisher,
					title: title
				});

				this.author.save().then(function(){
				  that.update();
				});

				book.save().then(function(book){
					// Return a promise with the book data to next .then()
				  return Parse.Promise.as(book);

				}).then(function(book){
					// Add the book to the author's book array of pointers
					that.author.addUnique('books', book);
					return that.author.save(); // Return promise to next .then()

				}).then(function(author) {
					that.books.push(book);
					that.bookTitle.value = ""; // Reset title input

				  that.update();
				});

			} else {
				alert('Needs an Author, Title, and Publisher');
			}
		};


		// I'm using the update hook to create copies of Parse Objects and
		// list of Parse Objects into simple JS objects via the toJSON() function
		this.on('update', function() {

			// Copy of Parse Object Data (shorter markup) using Underscore
			this.authorsData = _.map(this.authors, function(author) {
				return author.toJSON();
			});

			// See Underscore or LoDash for _.map() specifications
			this.publishersData = _.map(this.publishers, function(publisher) {
				return publisher.toJSON();
			});

			this.booksData = _.map(this.books, function(book) {
				// console.log(book.toJSON());
				return book.toJSON();
			});

			this.publisherData = this.publisher ? this.publisher.toJSON() : undefined;
			this.authorData = this.author ? this.author.toJSON() : undefined;

			// console.log(this.author, this.authorData);
		});

		this.getAuthors();
		this.getPublishers();
	</script>
</one-to-many>
