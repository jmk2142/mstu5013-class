<navbar>

	<div class={ loggedIn:Parse.User.current() } if={ Parse.User.current() }>
		<form>
			<span>Hello <strong>{ data.first } { data.last }</strong> ! You are logged in.</span>
			<button type="button" onclick={ logout }>LOGOUT</button>
		</form>
	</div>

	<form id="login" if={ !Parse.User.current() && showLogin }>
		<em>{ feedback ? feedback + " | " : "" }</em>
		<strong>LOGIN</strong>
		<input type="text" name="login-username" placeholder="username">
		<input type="password" name="login-password" placeholder="password">
		<button type="button" name="login-submit" onclick={ login }>LOGIN</button>
		<button type="button" name="login-gotoReg" onclick={ toggleLogin }>Goto Registration</button>
	</form>

	<form id="registration" if={ !Parse.User.current() && !showLogin }>
		<strong>REGISTRATION</strong>
		<input type="text" name="reg-username" placeholder="username">
		<input type="password" name="reg-password" placeholder="password">
		<input type="text" name="reg-email" placeholder="email">
		<input type="text" name="reg-first" placeholder="first name">
		<input type="text" name="reg-last" placeholder="last name">
		<button type="button" name="reg-submit" onclick={ register }>REGISTER</button>
		<button type="button" name="login-gotoReg" onclick={ toggleLogin }>Goto Login</button>
	</form>

	<script>
		var that = this;

		this.data = {};
		this.showLogin = true;
		this.feedback = '';

		this.logout = function(event) {
			this.showLogin = true;
		  Parse.User.logOut();
			observer.trigger('logOut');
		};

		this.toggleLogin = function(event) {
			this.feedback = "";
			this.showLogin = !this.showLogin;
		};

		this.login = function(event) {
			var username = this['login-username'].value;
			var password = this['login-password'].value;

		  Parse.User.logIn(username, password).then(function() {
				observer.trigger('logIn');
		    that.update();
		  }, function(error){
		    that.feedback = error.message;
				that.update();
		  });
		};

		this.register = function(event) {
			var username = this['reg-username'].value;
			var password = this['reg-password'].value;
			var email = this['reg-email'].value;
			var first = this['reg-first'].value;
			var last = this['reg-last'].value;

			Parse.User.signUp(username, password, {
				email: email,
				first: first,
				last: last
			}).then(function(user) {
				observer.trigger('logIn');
			  that.update();
			}, function(error) {
		    that.feedback = error.message;
				that.update();
		  });
		};

		this.on('update', function() {
			if (Parse.User.current()) {
				this.data = Parse.User.current().toJSON();
			} else {
				this.data = {};
			}
		});

	</script>

	<style scoped>
		:scope {
			display: block;
			width: 100%;
			background-color: #F44336;
		}

		form {
			padding: 15px;
			text-align: right;
		}

		.loggedIn {
			background-color: #8BC34A;
		}
	</style>
</navbar>
