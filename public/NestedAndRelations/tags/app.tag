<app>
	<div class="row">
		<div class="col-md-12">
			<br>
			<div class="alert alert-danger" if={ !Parse.User.current() }>
				<p>
					<strong>Make sure that you are logged in as we need an authenticated user for some examples.</strong>
				</p>
			</div>

			<div id="demo" show={ Parse.User.current() }></div>
		</div>
	</div>

	<script>
		var that = this;

		observer.on('logIn logOut', function(){
			that.update();
		});

	</script>
</app>
