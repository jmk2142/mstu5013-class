<app>
	<div id="appID">Welcome { user.first }</div>

	<div class="pieShelf">
		<pie each={ pies }></pie>
	</div>
	
	<script>
		console.log(this.opts);

		this.user = this.opts.user;
		this.pies = this.opts.pies;
	</script>

	<style>
		
		.pieShelf {
			border: 1px solid #333;
			padding: 10px;
		}
	</style>


</app>