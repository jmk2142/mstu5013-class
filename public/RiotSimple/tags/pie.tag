<pie>
	<div class="pie">
		<span onclick={ printMsg }>{ fruit } Pie </span>
		<button onclick={ removeFruit }>Remove</button>
	</div>

	<script>
		console.log(this);

		// Should have reread the section
		// on loops in the Guide.
		// Loops are special in the way they work.
		// The parent has control which helps with related children
		this.printMsg = function(event) {
			// WRONG WAY TO DO IT
			// this.fruit = "Strawberry";
			
			// Special event.item is used to reference the fruit object in this
			// child component that was passed in through an each loop
			event.item.fruit = 'Strawberry';
		};

		// Removing pies from parent pies array,
		// and automatically updating number of pie
		// components shown
		this.removeFruit = function(event) {
			var pie = event.item;
			var index = this.parent.pies.indexOf(pie);
			this.parent.pies.splice(index, 1);
		}
	</script>
</pie>