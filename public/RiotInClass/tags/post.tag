<post>
	<div class="post">
		A Post by <strong>{ cool ? authorName : 'Jin Kuwata' }</strong>
		
		<h3 onclick={ toggleEditTitle } if={ !editing }>{ title }</h3>
		<input name="titleInput" type="text" if={ editing } onkeypress={ setTitle } value={ title }>

		<p>{ message }</p>
		<button onclick={ destroyPost }>DESTROY</button>
	</div>


	<script>
		console.log('Post Tag:', this);
		console.log('Post opts:', this.opts);


		this.authorName = "Evin Watson";
		this.cool = true;

		this.editing = false;

		this.setTitle = function(event){
			if (event.which === 13) {
				event.item.title = this.titleInput.value;
				this.toggleEditTitle();
			}
			return true;
		}

		this.toggleEditTitle = function(event) {
			this.editing = !this.editing;
		};

		this.destroyPost = function(event){
			console.log(event);
			var post = event.item;

			// var index = this.posts.indexOf(post);
			// this.posts.splice(index, 1);

			this.posts.remove(post);

		};

	</script>

	<style scoped>
		:scope {
			display: block
		};
		.post {
			border: 1px solid blue;
			padding: 10px;
			margin: 15px 0;
		}
	</style>
</post>