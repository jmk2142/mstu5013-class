Parse.initialize("JOiRQ4zeHDhhGbzr4oKw9QmKyrlpTcIH6k6uNzXY", "pnXpWoBz1Vx65MB38SNOVjcHnnQYG6L0jEpnVisJ");
var Post = Parse.Object.extend('Post');

$('#send').click(function(event) {
	var myPost = new Post();
	myPost.save({
		message: $('#message').val()
	});

	addToChatroom(myPost.get('message'));
});

function addToChatroom(message) {
	$('#chatroom').prepend('<div class="post">' +  message + '</div>');
}

function printNewestMessages(dateObject) {
	var query = new Parse.Query('Post');
	query.greaterThan('createdAt', dateObject);
	query.descending('createdAt');

	query.find().then(function(results){
	  for(var i = 0; i < results.length; i++) {
	  	addToChatroom(results[i].get('message'));
	  }
	});
}