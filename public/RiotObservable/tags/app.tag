<app>
	
	<div clas="app">
		<login if={ !user }></login>
		<bakery if={ user } user={ user }></bakery>
	</div>


	<script>
		var that = this;
		this.user = Parse.User.current();

		// Listens to pubsub events loggedIn and loggedOut
		// Runs callback function if these events are fired.
		pubsub.on('loggedIn loggedOut', function(data) {
			that.user = Parse.User.current();
			that.update();
		});

	</script>

	<style>
		/* STYLES */
	</style>

</app>