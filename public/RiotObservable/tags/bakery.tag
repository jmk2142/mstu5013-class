<bakery>
	<div>
		<small>BAKERY</small>
		<button onclick={ logOut }>Logout</button>
		<strong>Hi { first } { last }</strong>
	</div>

	<script>
		var that = this;

		this.on('mount update unmount', function(eventName) {
			console.log(eventName + ': bakery', this);
		});

		this.on('update', function(eventName) {
			// console.log(eventName + ': bakery', this);
			this.user = this.opts.user;
			if (this.user) {
				this.first = this.user.get('first');
				this.last = this.user.get('last');
			}
		});

		this.logOut = function(event) {
			Parse.User.logOut();
			pubsub.trigger('loggedOut');
		};

	</script>
</bakery>