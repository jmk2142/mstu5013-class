// Initializing Parse
Parse.initialize("JOiRQ4zeHDhhGbzr4oKw9QmKyrlpTcIH6k6uNzXY", "pnXpWoBz1Vx65MB38SNOVjcHnnQYG6L0jEpnVisJ");

var Pet = Parse.Object.extend('Pet');

// Create a Parse.Object (our Model data)
$('#createPet').click(function(event) {

	// Here we use some shortcuts to create and set the values of our pet in one step
	var someNewPet = new Pet({
		typeOfPet: $('#petType').val(),
		nameOfPet: $('#petName').val()
	});

	// We use the promise way of getting our callbacks to happen in response to server
	// Object.save() will return a Promise Object

	// Promise.then(success, error);

	someNewPet.save().then(function(pet) {
		// this first function arg is the success callback
		// the success callback function can take an argument which represents the saved object

		var petView = new PetView({
			model: pet
		});

		$('#petViewer').prepend(petView.render().el);
		

	}, function(error) {
		// this second function arg is the error callback
		// the success callback function can take an argument which represents the error object
		
		alert(error.message);
	});
});




var PetView = Backbone.View.extend({
	// You can decide what HTML tags or classes, etc.
	tagName: 'div',
	className: 'pet clearfix',

	// This lets us delegate events to parts HTML within this view.
	events: {
		'click .deletePet': 'destroyPet'
	},

	/*
		A couple things going on here. First, the _.template() function.
		The _.template() function takes one argument, a string representation of a template.
		So something like _.template("<li>My List Item</li>") is perfectly okay.
		We are using jQuery to get that string from our page instead of writing it out here.
		See $('#petTEMPLATE') in html now...

		What the _.template() function returns is another function... a function that can
		take a data object to produce specific variations of the template.
		
		PetView.makeHTML is a function created by the _.template() function.
		PetView.makeHTML can take a data argument that fills the template blanks

		See render()
	*/
	makeHTMLStr: _.template( $('#petTEMPLATE').html() ),

	// This is code that always runs first thing a new PetView is instantiated
	initialize: function() {
		console.log('Pet View created.');
	},
	render: function() {
		var data = this.model.attributes; // -> {nameOfPet:"something", typeOfPet:"another"}
		var compiled = this.makeHTMLStr(data); // -> "<span>This pet is a <em>another</em> ... ... ... </span>"

		this.$el.html(compiled); // -> Changes this view's el to reflect compiled HTML

		return this;
	},


	destroyPet: function() {
		this.model.destroy();
		this.remove();
	}
});