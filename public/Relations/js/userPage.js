Parse.initialize("8shSBt0nS2avoZGq1G9atK7s6QQTh2KpEeNAPWR4", "Syk8Nz51z5GaXqIeZyJOgl0cfRYb4H3Db7aOQO13");

// From https://css-tricks.com/snippets/javascript/get-url-variables/
function getParam(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		if (pair[0] == variable) {
			return pair[1];
		}
	}
	return (false);
}

// We passed in the userID in the URL
// We grab that value from the URL with this function
var userID = getParam('userID');

// We create a generic Parse.User object
var user = new Parse.User();
// We explicitly assign it the ID that we passed in
// Note, the user object will have no real values except an id which we assigned manually
user.id = userID;
console.log('Empty User (Shell):', user);

// We can 'rehydrate' this object using fetch() since we know and have assigned an ID
user.fetch().then(function(user) {
	console.log('Hydrated User:', user);
});

// But since we also passed in some things like first and last name in the URL parameters...
// We can use those instantly without waiting for a network call and response.

var first = getParam('first');
var last = getParam('last');

$('.userFirst').html(first);

// We do want a list of all books user likes...

var bookQuery = new Parse.Query('Book');
		// books where user is contained in the array property 'likes'
		bookQuery.containedIn('likes', [user]);
		// don't forget to include the users in likes by using the include() query function
		// by default queries do not include child objects to save bandwidth
		bookQuery.include('likes');

bookQuery.find().then(function(results) {
	// For results, which is an array of books...
	_.each(results, function(book) {

		var bookView = new BookView({
			model: book
		});

		$('#books').append(bookView.render().el);
	});
});
