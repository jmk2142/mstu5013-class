var Book = Parse.Object.extend('Book', {
	defaults: {
		author: 'Anonymous',
		title: 'No Title',
		published: '1970',
		likes: []
	},
	initialize: function() {
		console.log('book initialiized');
	},
	getAPAHTML: function() {
		return this.model('author') + '(' + this.model.get('published') + '), <em>' + this.model.get('title') + '</em>;';
	}
});