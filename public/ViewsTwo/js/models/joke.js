var Joke = Parse.Object.extend('Joke', {
	defaults: {
		author: 'anonymous',
		joke: 'No joke? Is this the joke?',
		punchline: null,
		score: 0
	},
	initialize: function() {
		console.log('joke initialized');
	}
});