var Navbar = Parse.View.extend({
	events: {
		'click .refresh': 'refreshContent'
	},
	initialize: function() {
		console.log('navbar initialized');
	},
	refreshContent: function() {
		fetchJokes();
	}
});