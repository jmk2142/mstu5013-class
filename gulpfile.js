var gulp = require('gulp');
var livereload = require('gulp-livereload');
var webserver = require('gulp-webserver');
// var inject = require('gulp-inject');
// var debug = require('gulp-debug');

gulp.task('webserver', function() {
	return gulp.src('public')
		.pipe(webserver({
			livereload: true,
			open: true
		}));
});

// gulp.task('inject', function(){
// 	var target = gulp.src('index.html');
// 	var tagSources = gulp.src(['tags/**/*.tag'], {read:false});
// 	var jsSources = gulp.src(['js/**/*.js'], {read:false});
//
// 	return target.pipe(inject(tagSources, {
// 		starttag: '<!-- inject:{{ext}} -->',
// 		transform: function(filepath, file, i, length) {
// 		  return '<script type="riot/tag" src="' + filepath + '"></script>';
// 		}
// 	})).pipe(inject(jsSources))
// 		.pipe(gulp.dest('.'))
// 		.pipe(livereload());
// });

gulp.task('reload', function() {
	return gulp.pipe(livereload());
});

gulp.task('watch', ['webserver'], function() {
	livereload.listen();
	gulp.watch('public/**/*.tag', function(){
	  livereload.reload();
	});
	gulp.watch('public/**/*.js', function(){
	  livereload.reload();
	});
	gulp.watch('public/**/*.html', function(){
	  livereload.reload();
	});
});
